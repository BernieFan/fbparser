package ntou.FBParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class App {
	public static void main(String[] args) {
		Output output = new Output();
		String[] filenames = {"1.txt", "2.txt", "3.txt", "4.txt", "5.txt" , "6.txt", "7.txt", "8.txt", "9.txt", "10.txt"};
		for(int i = 0; i < 10; i++){
		    System.out.println("Parse Group" + (i+1));
			output.insert(i, extractFB("D:/fb/"+filenames[i]));
		}
	    System.out.println("Output file");
		output.wirteFile();
	}

	public static ArrayList<ArrayList<String>> extractFB(String filename) {
		ArrayList<ArrayList<String>> all = new ArrayList<ArrayList<String>>();
		try {
			String html = readFile(filename);
			Document doc = Jsoup.parse(html);

			Elements posts = doc.select("div[class*=_5jmm]");
			// Sorting by post time
			Collections.sort(posts, new Comparator<Element>() {
				public int compare(Element a, Element b) {
					String timeA = a.select("abbr").get(0).attr("data-utime");
					String timeB = b.select("abbr").get(0).attr("data-utime");
					return timeA.compareTo(timeB);
				}
			});

			Output output = new Output();
			for (Element item : posts) {
				all.addAll(extractPost(item));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return all;
	}

	public static ArrayList<ArrayList<String>> extractPost(Element doc)
			throws IOException {
		Elements postAuthor = doc.select("span[class*=fwb] a");
		Elements postContent = doc.select("p");
		Elements postTime = doc.select("abbr");
		Elements commentAuthor = doc.select("a[class*=UFICommentActorName]");
		Elements commentBody = doc.select("span[class*=UFICommentBody]");

		ArrayList<ArrayList<String>> sources = new ArrayList<ArrayList<String>>();
		ArrayList<String> temp = new ArrayList<String>();
		temp.add(postTime.get(0).attr("title"));
		temp.add(postAuthor.text());
		temp.add(postContent.text());
		sources.add(temp);

		// System.out.printf("%s\t%s\t%s\t", postTime.get(0).attr("title"),
		// postAuthor.text(), postContent.text());

		for (int i = 0; i < commentAuthor.size(); i++) {

			if (i == 0) {
				// System.out.printf("\t%s\t%s\t%s\n",
				// postTime.get(i + 1).attr("title"), commentAuthor.get(i)
				// .text(), commentBody.get(i).text());
				temp = sources.get(0);
				temp.add(postTime.get(i + 1).attr("title"));
				temp.add(commentAuthor.get(i).text());
				temp.add(commentBody.get(i).text());
			} else {
				// System.out.printf("\t\t\t%s\t%s\t%s\n",
				// postTime.get(i + 1).attr("title"), commentAuthor.get(i)
				// .text(), commentBody.get(i).text());
				temp = new ArrayList<String>();
				temp.add("");
				temp.add("");
				temp.add("");
				temp.add(postTime.get(i + 1).attr("title"));
				temp.add(commentAuthor.get(i).text());
				temp.add(commentBody.get(i).text());
				sources.add(temp);
			}
		}

		// if (commentAuthor.size() == 0)
		// sources.add(new ArrayList<String>());
		// System.out.println();
		return sources;
	}

	public static String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}
}
